
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import playerData from'./playerData.js';

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Custom event handler
window.Event = new Vue();

// Storage container with default values
let Store = {
    mode: "",
    players: [],
};

// Mode selector
Vue.component('mode', {
    template: `<li @click="emitMode">{{ mode }}</li>`,
    props: {
        mode: String,
    },
    methods: {
        emitMode() {
            Event.$emit('selectMode', this.mode);
        }
    }
});

Vue.component('mode-selector', {
    template: `<ul><mode v-for="mode in modes" :mode="mode.mode" :key="mode.mode">{{ mode.mode }}</mode></ul>`,
    data() {
        return {
            modes: [
                { mode:'player' },
                { mode:'overview' },
            ],
        }
    }
});

new Vue({
    el: '#mode-selector',
    template: `<mode-selector v-show="!Store.mode"></mode-selector>`,
    created() {
        Event.$on('selectMode', (mode)=>{
            Store.mode = mode;
        });
    },
    data() {
        return {
            Store: Store
        }
    }
});

// Player selector
Vue.component('player', {
    template: `<li @click="emitPlayer">{{ player.name }}</li>`,
    props: {
        player: Object,
    },
    methods: {
        emitPlayer() {
            Event.$emit('selectPlayer', this.player);
        }
    },
    data() {
        return {
            Store: Store
        }
    }
});

Vue.component('player-selector', {
    template: `
        <ul v-if="checkIfDisplays()">
            <player v-for="player in players" :player="player" :key="player.name"></player>
        </ul>
    `,
    props: {
        players: Array,
    },
    methods: {
        checkIfDisplays() {
            let playerSelected = this.Store.mode == 'player' && this.Store.players.length <= 0;
            return playerSelected || (this.Store.mode == 'overview');
        }
    },
    data() {
        return {
            Store: Store
        }
    }
});

new Vue({
    el: '#player-container',
    template: `<player-selector :players="players"></player-selector>`,
    created(){
        Event.$on('selectPlayer', (player) => {
            console.log('player added', player);
            Store.players.push(player);
        })
    },
    data() {
        return {
            players: playerData.players,
        }
    }
});

// Player tiles and sliders
Vue.component('number', {
    template: `
        <span v-bind:class="determineColor(currentIndex, index)">{{ number }}</span>
    `,
    props: {
        index: Number,
        currentIndex: Number,
        number: Number,
    },
    methods: {
        determineColor(currentIndex, index) {
            if(currentIndex == 0) {
                Event.$emit('playerDead');
            }
            if(index == 0) {
                return 'danger';
            }
            if(currentIndex == index) {
                return 'selected';
            }
        }
    }
})

Vue.component('slider', {
    template:`
        <div class="slider-container">
            <h3>{{ name }}</h3>
            <button @click="moveIndex(-1)">-</button>
            <number v-for="(num, pointer) in stat" :index="pointer" :currentIndex="currentIndex" :number="num"></number>
            <button @click="moveIndex(1)">+</button>
        </div>
    `,
    props: {
        index: Number,
        stat: Array,
        name: String,
    },
    methods: {
        moveIndex(amount) {
            this.currentIndex += amount;
        }
    },
    data() {
        return {
            currentIndex: this.index
        }
    }
});

Vue.component('tile', {
    template: `
        <div class="tile" v-bind:class="checkIfDead()">
            <h2 class="character-name" v-bind:class="player.color">{{ player.name }}</h2>
            <slider :index="player.speedStartIndex" :stat="player.speed" name="Speed"></slider>
            <slider :index="player.mightStartIndex" :stat="player.might" name="Might"></slider>
            <slider :index="player.sanityStartIndex" :stat="player.sanity" name="Sanity"></slider>
            <slider :index="player.knowledgeStartIndex" :stat="player.knowledge" name="Knowledge"></slider>
        </div>
    `,
    created() {
        Event.$on('playerDead', ()=> {
            this.player.alive = false;
        })
    },
    methods: {
        checkIfDead() {
            if(!this.player.alive) {
                return "dead";
            }
        }
    },
    props: {
        player: Object
    },
});

new Vue({
    el: '#tile-container',
    template: '<div><tile v-for="player in Store.players" :key="player.name" :player="player"></tile></div>',
    data() {
        return {
            Store: Store
        }
    }
});
