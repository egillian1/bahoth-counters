// Statistics and names for each of the playable characters

let players  = [
    {
        name: "Missy Dubourde",
        alive: true,
        color: "orange",
        speedStartIndex: 3,
        speed: [0, 3, 4, 5, 6, 6, 6, 7, 7],
        mightStartIndex: 4,
        might: [0, 2, 3, 3, 3, 4, 5, 6, 7],
        sanityStartIndex: 3,
        sanity: [0, 1, 2, 3, 4, 5, 5, 6, 7],
        knowledgeStartIndex: 4,
        knowledge: [0, 2, 3, 4, 4, 5, 6, 6, 6]
    },
    {
        name: "Father Rhinehardt",
        alive: true,
        color: "white",
        speedStartIndex: 3,
        speed: [0, 2, 3, 3, 4, 5, 6, 7, 7],
        mightStartIndex: 3,
        might: [0, 1, 2, 2, 4, 4, 5, 5, 7],
        sanityStartIndex: 5,
        sanity: [0, 3, 4, 5, 5, 6, 7, 7, 8],
        knowledgeStartIndex: 4,
        knowledge: [0, 1, 3, 3, 4, 5, 6, 6, 8]
    },
];

export default {
    players: players
};
