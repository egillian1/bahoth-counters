<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BaHotH Counters</title>

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="/css/app.css">
    </head>
    <body>
        <div class="content">
            <div class="title m-b-md">
                BaHotH Counters
            </div>

            <div class="selectors">
                <div id="mode-selector"></div>

                <div id="player-container"></div>

                <div id="tile-container"></divid>
            </div>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
